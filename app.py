import numpy as np
from flask import Flask
app = Flask(__name__)
app.debug=True

# def hello_world():
#     return 'hello all'

def cal_integration(lower_bound, upper_bound, N):
    x = np.linspace(lower_bound, upper_bound, N)
    y = abs(np.sin(x))
    dx = x[1] - x[0]
    return np.sum(y*dx)

def test_integration(lb, ub):
    result = []
    for n in [10, 100, 1000, 10000, 100000, 1000000, 10000000]:
        val = cal_integration(lb, ub, n)
        result.append(str(val))
    return ' >> '.join(result)

@app.route('/')
def test():
    return test_integration(0, 314)

# @app.route('/')
# def test():
#     return str(integration(0, 3.14, 1000))    
